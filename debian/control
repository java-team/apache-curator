Source: apache-curator
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Emmanuel Bourg <ebourg@apache.org>
Build-Depends:
 ant,
 debhelper-compat (= 13),
 default-jdk,
 javahelper,
 maven-debian-helper
Build-Depends-Indep: junit5,
 libactivation-java,
 libassertj-core-java,
 libbuild-helper-maven-plugin-java,
 libcommons-math-java,
 libdropwizard-metrics-java,
 libguava-java,
 libjackson-json-java,
 libjackson2-core-java,
 libjackson2-databind-java,
 libjackson2-dataformat-yaml,
 libjavassist-java,
 libjaxb-api-java,
 libjaxb-java,
 libjersey1-client-java,
 libjersey1-core-java,
 libjersey1-server-java,
 libjersey1-servlet-java,
 libjsr311-api-java,
 libmaven-antrun-plugin-java,
 libmaven-bundle-plugin-java,
 libmaven-dependency-plugin-java,
 libmockito-java,
 libscannotation-java,
 libsnappy-java,
 libslf4j-java,
 libzookeeper-java (>= 3.8.0)
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/java-team/apache-curator.git
Vcs-Browser: https://salsa.debian.org/java-team/apache-curator
Homepage: https://curator.apache.org
Rules-Requires-Root: no

Package: libcurator-parent-java
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
Description: Apache Curator Parent
 Curator is a set of Java libraries that make using Apache ZooKeeper much
 easier.
 .
 This package contains the parent POM of the Curator components.

Package: libcurator-client-java
Architecture: all
Depends: ${misc:Depends}, libcurator-parent-java, ${java:Depends}
Description: Apache Curator Client
 Curator is a set of Java libraries that make using Apache ZooKeeper much
 easier.
 .
 This package contains the Curator Client which is a low-level API wrapping
 the ZooKeeper's Java client. It makes client access to ZooKeeper much simpler
 and less error prone. It provides the following features:
  * Continuous connection management
  * Operation retry utilities
  * In-process, self-contained, Test ZooKeeper server

Package: libcurator-framework-java
Architecture: all
Depends: ${misc:Depends}, libcurator-parent-java, ${java:Depends}
Description: Apache Curator Framework
 Curator is a set of Java libraries that make using Apache ZooKeeper much
 easier.
 .
 This package contains the Curator Framework which is a high-level API that
 greatly simplifies using ZooKeeper. It adds many features that build on
 ZooKeeper and handles the complexity of managing connections to the ZooKeeper
 cluster and retrying operations.

Package: libcurator-test-java
Architecture: all
Depends: ${misc:Depends}, libcurator-parent-java, ${java:Depends}
Description: Apache Curator Testing Utilities
 Curator is a set of Java libraries that make using Apache ZooKeeper much
 easier.
 .
 This package contains the testing server, the testing cluster and a few other
 tools useful for testing.

Package: libcurator-recipes-java
Architecture: all
Depends: ${misc:Depends}, libcurator-parent-java, ${java:Depends}
Description: Apache Curator Recipes
 Curator is a set of Java libraries that make using Apache ZooKeeper much
 easier.
 .
 This package contains the implementations of some of the common ZooKeeper
 "recipes" (elections, locks, barriers, counters, caches, nodes and queues).
 The implementations are built on top of the Curator Framework.

Package: libcurator-discovery-java
Architecture: all
Depends: ${misc:Depends}, libcurator-parent-java, ${java:Depends}
Description: Apache Curator Service Discovery
 Curator is a set of Java libraries that make using Apache ZooKeeper much
 easier.
 .
 This package contains a service discovery implementation built
 on the Curator Framework.
